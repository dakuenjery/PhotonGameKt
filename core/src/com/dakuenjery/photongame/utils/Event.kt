package com.dakuenjery.photongame.utils

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.ContactImpulse

/**
 * Created by dakue on 29/08/16.
 */

sealed class Event {
    class DestroyBox(position: Vector2, val impulse: ContactImpulse) : Event() {
        val position = Vector2(position)
    }

    class PhotonTouched(position: Vector2, val impulse: ContactImpulse) : Event() {
        val position = Vector2(position)
    }
}

enum class GameEvent {
    StartGame
}