package com.dakuenjery.photongame.utils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.VertexAttribute
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.ContactImpulse
import com.badlogic.gdx.physics.box2d.Manifold
import com.badlogic.gdx.utils.Array
import com.dakuenjery.photongame.GameConsts
import com.sun.org.apache.xpath.internal.operations.Bool
import java.util.*
import java.util.Random
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Created by dakue on 16/03/16.
 */

object Random {
    private val vector = Vector2()
    private val random = Random()

//    fun float() : Float = random.nextFloat()
    fun float(start: Float = -1f, end: Float = 1f): Float = random.nextFloat() * (end-start) + start
    fun int(end: Int = Int.MAX_VALUE): Int = random.nextInt(end)
    fun int(start: Int, end: Int): Int = int(end) + start

    fun vector(start: Float = -1f, end: Float = 1f): Vector2 =
        vector.set(float(start, end), float(start, end))

    fun vector(int1a: Float, int1b: Float, int2a: Float, int2b: Float, i: Int = int(3)): Vector2 {

        when(i) {
            0 -> vector.set(float(int1a, int1b), float(int1a, int1b))
            1 -> vector.set(float(int2a, int2b), float(int1a, int1b))
            2 -> vector.set(float(int2a, int2b), float(int2a, int2b))
            3 -> vector.set(float(int1a, int1b), float(int2a, int2b))
        }
        return vector
    }

    fun color(a: Float = 1f): Color = Color(float(), float(), float(), a)
    fun lightColor(a: Float = 1f): Color =
            Color(float(0.7f, 1f), float(0.7f, 1f), float(0.7f, 1f), a)
    fun darkColor(a: Float = 1f): Color =
            Color(float(0.1f, 0.5f), float(0.1f, 0.5f), float(0.1f, 0.5f), a)
    fun redColor(a: Float = 1f): Color =
            Color(float(0.7f, 1f), float(0.1f, 0.4f), float(0.1f, 0.4f), a)
    fun greenColor(a: Float = 1f): Color =
            Color(float(0.1f, 0.4f), float(0.7f, 1f), float(0.1f, 0.4f), a)
    fun blueColor(a: Float = 1f): Color =
            Color(float(0.1f, 0.4f), float(0.1f, 0.4f), float(0.7f, 1f), a)
}

object CoordHelper {
    val vec3: Vector3 = Vector3()
    val vec1: Vector2 = Vector2()
    val vec2: Vector2 = Vector2()

    fun init(cam: Camera, worldWidth: Float) {

    }

    private val transformScreenCenter = Vector2(Gdx.graphics.width.toFloat()/2, -Gdx.graphics.height.toFloat()/2)

    fun screenToRelative(cam: Camera, x: Float, y: Float): Vector2 {
        cam.unproject(vec3.set(x, y, 0f))
        return vec1.set(vec3.x, vec3.y).scl(vec3.z)
    }

    fun relativeToWorld(vec: Vector2, pos: Vector2, scale: Float = 10f): Vector2 {
        return vec.set(pos).scl(scale)
    }

    fun relativeToWorld(x: Float, y: Float, scale: Float = 10f): Vector2 =
            relativeToWorld(vec1, vec2.set(x, y), scale)

    fun relativeToWorld(pos: Vector2, scale: Float = 10f): Vector2 =
            relativeToWorld(vec1, pos, scale)


    fun worldToRelative(vec: Vector2, x: Float, y: Float, scale: Float = 10f): Vector2 =
            vec.set(x, y).scl(1f/scale)
}

object ColorHalper {
    private val mnf = Vector3()

    fun fromHSV(rgb: Color, hsv: Vector3): Color {
        hsv.x = hsv.x % 6

        val i: Int = MathUtils.floor(hsv.x).toInt()

        mnf.z = hsv.x - i

        if (i % 2 == 0)
            mnf.z = 1 - mnf.z

        mnf.x = hsv.z * (1 - hsv.y)
        mnf.y = hsv.z * (1 - hsv.y * mnf.z)

        when (i) {
            0 -> {
                rgb.r = hsv.z
                rgb.g = mnf.y
                rgb.b = mnf.x
            }
            1 -> {
                rgb.r = mnf.y
                rgb.g = hsv.z
                rgb.b = mnf.x
            }
            2 -> {
                rgb.r = mnf.x
                rgb.g = hsv.z
                rgb.b = mnf.y
            }
            3 -> {
                rgb.r = mnf.x
                rgb.g = mnf.y
                rgb.b = hsv.z
            }
            4 -> {
                rgb.r = mnf.y
                rgb.g = mnf.x
                rgb.b = hsv.z
            }
            5 -> {
                rgb.r = hsv.z
                rgb.g = mnf.x
                rgb.b = mnf.y
            }
        }

        return rgb
    }
}

object MathHelper {
    private val tmpVec = Vector2()
    private val tmpVec2 = Vector2()

    fun cosAngleOy(data: kotlin.Array<Float>, offset: Int): Float =
            cosAngleOy(data[offset], data[offset+1], data[offset+2], data[offset+3])

    fun cosAngleOy(x1: Float, y1: Float, x2: Float, y2: Float): Float =
            cosAngle(x2-x1, y2-y1, 1f, 0f)

    fun cosAngleOy(vec1: Vector2, vec2: Vector2): Float =
            cosAngleOy(vec1.x, vec1.y, vec2.x, vec2.y)

    fun cosAngleOx(data: kotlin.Array<Float>, offset: Int): Float =
            cosAngleOx(data[offset], data[offset+1], data[offset+2], data[offset+3])

    fun cosAngleOx(x1: Float, y1: Float, x2: Float, y2: Float): Float =
            cosAngle(x2-x1, y2-y1, 0f, 1f)

    fun cosAngleOx(vec1: Vector2, vec2: Vector2): Float =
            cosAngleOx(vec1.x, vec1.y, vec2.x, vec2.y)

    fun cosToSin(cos: Float): Float =
            Math.sqrt(1 - (cos*cos).toDouble()).toFloat()

    fun cosAngle(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        val dot = x1*x2 + y1*y2
        val l1 = length(x1, y1)
        val l2 = length(x2, y2)
        return dot / (l1*l2)
    }

    fun cosAngle(vec1: Vector2, vec2: Vector2): Float =
            cosAngle(vec1.x, vec1.y, vec2.x, vec2.y)

    fun length(x1: Float, y1: Float): Float =
            Math.sqrt((x1*x1 + y1*y1).toDouble()).toFloat()

    fun length(vec: Vector2): Float =
            length(vec.x, vec.y)

    fun distanse(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        val x = (x2 - x1)
        val y = (y2 - y1)
        return length(x, y)
    }

    fun distanse(vec1: Vector2, vec2: Vector2): Float =
            distanse(vec1.x, vec1.y, vec2.x, vec2.y)

    fun middle(tempVector: Vector2, x1: Float, y1: Float, x2: Float, y2: Float): Vector2 {
        val x = x1 + x2
        val y = y1 + y2
        return tempVector.set(x/2, y/2)
    }

    fun middle(tempVector: Vector2, p1: Vector2, p2: Vector2): Vector2 =
            middle(tempVector, p1.x, p1.y, p2.x, p2.y)

    fun middle(x1: Float, y1: Float, x2: Float, y2: Float): Vector2 =
            middle(Vector2(), x1, y1, x2, y2)

    fun middle(p1: Vector2, p2: Vector2): Vector2 =
            middle(p1.x, p1.y, p2.x, p2.y)

}

class MeshHelper(val points: Int = 4) {
    val LINESIZE = 3+4

    val floatArr = FloatArray(LINESIZE * points, { 1f })
    val short6Ind = shortArrayOf(0, 1, 2, 2, 3, 0)

    val shortInd = ArrayList<Short>()

    var line = 0

    private var vertexAdded = 0

    fun set(vararg args: Float): MeshHelper {
        var ind = LINESIZE * line++

        for (arg in args)
            floatArr[ind++] = arg

        vertexAdded++

        return this
    }

    fun set(pos: Vector2): MeshHelper {
        var ind = LINESIZE * line++

        floatArr[ind++] = pos.x
        floatArr[ind++] = pos.y
        floatArr[ind] = 0f

//        Gdx.app.log("MeshHelper.set", pos.toString())

        vertexAdded++

        return this
    }

    fun set(pos: Vector3): MeshHelper {
        var ind = LINESIZE * line++

        floatArr[ind++] = pos.x
        floatArr[ind++] = pos.y
        floatArr[ind] = pos.z

        vertexAdded++

        return this
    }

    fun index(): MeshHelper {
        if (vertexAdded == 3) {
            for (i in 0..2)
                shortInd.add(short6Ind[i])
        } else if (vertexAdded == 4) {
            for (i in 0..5)
                shortInd.add(short6Ind[i])
        } else
            return this

//        Gdx.app.log("MeshHelper.index()", "$vertexAdded \n")

        for (i in 0..short6Ind.size-1)
            short6Ind[i] = (short6Ind[i] + vertexAdded).toShort()

        vertexAdded = 0

        return this
    }

    fun create(): Mesh {
        val mesh = Mesh(true, points, shortInd.size, VertexAttribute.Position(), VertexAttribute.ColorUnpacked())
        mesh.setVertices(floatArr)
        mesh.setIndices(shortInd.toShortArray())
        return mesh
    }
}

object GlHelper {
    private val LINESIZE = 3+4
    private val POINTS = 4

    private val floatArr7x4 = FloatArray(LINESIZE * POINTS, { 1f })
    private val short6Ind = shortArrayOf(0, 1, 2, 2, 3, 0)

    fun createMesh(floatArr8: FloatArray): Mesh {
        val mesh = Mesh(true, 4, 6, VertexAttribute.Position(), VertexAttribute.ColorUnpacked())
        mesh.setVertices(vertex7x4(floatArr8))
        mesh.setIndices(short6Ind)

        return mesh
    }

    private fun vertex7x4(floatArr8: FloatArray): FloatArray {
        floatArr7x4[LINESIZE * 0 + 0] = floatArr8[6]
        floatArr7x4[LINESIZE * 0 + 1] = floatArr8[7]
        floatArr7x4[LINESIZE * 0 + 2] = 0f

        floatArr7x4[LINESIZE * 1 + 0] = floatArr8[4]
        floatArr7x4[LINESIZE * 1 + 1] = floatArr8[5]
        floatArr7x4[LINESIZE * 1 + 2] = 0f

        floatArr7x4[LINESIZE * 2 + 0] = floatArr8[2]
        floatArr7x4[LINESIZE * 2 + 1] = floatArr8[3]
        floatArr7x4[LINESIZE * 2 + 2] = 0f

        floatArr7x4[LINESIZE * 3 + 0] = floatArr8[0]
        floatArr7x4[LINESIZE * 3 + 1] = floatArr8[1]
        floatArr7x4[LINESIZE * 3 + 2] = 0f

        return floatArr7x4
    }
}

class Tasker() {
    private val taskQueue = ConcurrentLinkedQueue<() -> Boolean>()
    private var iter = 0

    fun addTask(task: () -> Boolean) = taskQueue.add(task)
    fun proceed() {
        val iter = taskQueue.iterator()
        while (iter.hasNext()) {
            val f = iter.next()
            if (f.invoke())
                iter.remove()
        }
    }

    fun proceedIfNeed(iter: Int) {
        if (this.iter++ % iter == 0)
            proceed()
    }
}

open class Observer<T> {
    protected val subjects = Array<T>(16)

    fun addSubject(value: T) = subjects.add(value)
    fun addSubjects(vararg values: T) = values.forEach { addSubject(it) }
    fun removeSubject(value: T) = subjects.removeValue(value, true)
}