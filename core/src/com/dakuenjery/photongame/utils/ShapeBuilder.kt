package com.dakuenjery.photongame.utils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Array

/**
 * Created by dakue on 16/04/16.
 */

class ShapeBuilder(private val capacity: Int = 100, private val thick: Float = 0.07f) {

    private val arr = FloatBuffer(capacity)

    private val prevPoint = Vector2()
    private val newPoint = Vector2()
    private val prevAngles = Vector2()
    private var prevOxAngle = 0f

    val pointCount: Int
        get() = arr.size/4

    fun extractData(shapeId: Int = pointCount-2): ArrayProvider {
        val index = shapeId * 4

        if (index < 0 || index + 8 > arr.size)
            throw IndexOutOfBoundsException()

        return ArrayProvider(arr.data, index, 8)
    }

    fun add(point: Vector2): Float {

        var dist = MathHelper.distanse(prevPoint, point)

        if (arr.size == 0) {
            initArr(point)
            prevPoint.set(point)

            return dist
        }

        // too near points
        if (prevPoint.epsilonEquals(point, 0.2f))
            return 0f

        val angleOx = MathHelper.cosAngleOx(prevPoint, point)

        if (isEquivalentAngles(angleOx, prevOxAngle, 0.5f) and false) {
            val distance = MathHelper.distanse(prevPoint, point)

            val cos = prevOxAngle * MathUtils.degreesToRadians
            val sin = MathHelper.cosToSin(prevOxAngle) * MathUtils.degreesToRadians

            newPoint.set(prevPoint).add(distance * cos, distance * sin)

            val angle = MathHelper.cosAngleOx(prevPoint, newPoint)

            appendPoint(newPoint, prevAngles.x, prevAngles.y)

            prevPoint.set(newPoint)
            prevOxAngle = angleOx
        } else {
            val angleOy = MathHelper.cosAngleOy(prevPoint, point)

            val xOffset = angleOx * thick
            val yOffset = angleOy * thick

            appendPoint(point, xOffset, yOffset)

            prevAngles.set(xOffset, yOffset)
            prevPoint.set(point)
            prevOxAngle = angleOx
        }

        return dist
    }

    private fun appendPoint(point: Vector2, xOffset: Float, yOffset: Float) {
        arr[arr.size - 4] = prevPoint.x - xOffset
        arr[arr.size - 3] = prevPoint.y + yOffset
        arr[arr.size - 2] = prevPoint.x + xOffset
        arr[arr.size - 1] = prevPoint.y - yOffset

        arr[arr.size + 0] = point.x + xOffset
        arr[arr.size + 1] = point.y - yOffset
        arr[arr.size + 2] = point.x - xOffset
        arr[arr.size + 3] = point.y + yOffset

        arr.size += 4

        if (arr.capacity - arr.size < 4)
            arr.increaseBuffer()
    }

    private fun initArr(point: Vector2) {
        arr[0] = point.x
        arr[1] = point.y
        arr[2] = point.x
        arr[3] = point.y

        arr.size += 4
    }

    private fun isEquivalentAngles(a: Float, b: Float, epsilon: Float): Boolean =
            Math.abs(a-b) < epsilon

    fun log() {
//        Gdx.app.log("ShapeBuilder.add", "${arr[arr.size-4]}, ${arr[arr.size-3]}, ${arr[arr.size-2]}, ${arr[arr.size-1]}")
    }
}

class FloatBuffer(private val cap: Int) {
    var data = FloatArray(cap)
    var size: Int = 0
    val capacity: Int
        get() = data.size

    operator fun get(index: Int): Float = data[index]
    operator fun set(index: Int, value: Float) {
        data[index] = value
    }

    fun increaseBuffer(incSize: Int = cap) {
        val newData = FloatArray(size + incSize)
        System.arraycopy(data, 0, newData, 0, size)
        data = newData
    }
}

data class ArrayProvider(
        val data: FloatArray,
        val offset: Int, val lenght: Int
)
