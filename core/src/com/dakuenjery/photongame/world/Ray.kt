package com.dakuenjery.photongame.world


import com.badlogic.gdx.graphics.g3d.Renderable
import com.dakuenjery.photongame.objects.Component
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Pool
import com.dakuenjery.photongame.objects.GameObject
import com.dakuenjery.photongame.utils.*

/**
 * Created by dakue on 17/03/16.
 */

class Ray(val world: World) : Component() {
    private val coords = Vector2()

    private val body = GameObject.DinamicBody(world)

    private val MAXPOINTS = 300
    private val RAYLENGHT = 10f

    private var rayLenght = 0f

    private val curve = ShapeBuilder()

    init {
        body.body.setTransform(Vector2.Zero, 0f)
    }

    override fun getRenderables(renderables: Array<Renderable>?, pool: Pool<Renderable>?) {
        body.getRenderables(renderables, pool)
    }

    fun addPoint(addPoint: Vector2) {
        CoordHelper.relativeToWorld(coords, addPoint)

        if (rayLenght > RAYLENGHT)
            return

        val dist = curve.add(coords)

        if (dist == 0f)
            return

        if (curve.pointCount < 2)
            return

        if (curve.pointCount > 2) {
            body.removeFixture()

            var data = curve.extractData(curve.pointCount-3)
            body.addFixture(data.data, data.offset, data.lenght)

            data = curve.extractData(curve.pointCount-2)
            body.addFixture(data.data, data.offset, data.lenght)
        } else {
            body.body.isActive = true
            val (data, offset, len) = curve.extractData()
            body.addFixture(data, offset, len)
        }

        body.updateModel()

        rayLenght += dist
    }
}