package com.dakuenjery.photongame.world

import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.ContactImpulse
import com.badlogic.gdx.physics.box2d.Manifold
import com.dakuenjery.photongame.objects.GameObject

/**
 * Created by dakue on 27/08/16.
 */


abstract class ContactListener : com.badlogic.gdx.physics.box2d.ContactListener {
    open fun beginContact(obj1: GameObject, obj2: GameObject) {}
    open fun endContact(obj1: GameObject, obj2: GameObject) {}
    open fun preSolve(obj1: GameObject, obj2: GameObject) {}
    open fun postSolve(obj1: GameObject, obj2: GameObject, impulse: ContactImpulse) {}

    final override fun endContact(contact: Contact?) {
        val obj1 = contact!!.fixtureA.body.userData
        val obj2 = contact.fixtureB.body.userData

        if (obj1 is GameObject && obj2 is GameObject)
            endContact(obj1 as GameObject, obj2 as GameObject)
    }

    final override fun beginContact(contact: Contact?) {
        val obj1 = contact!!.fixtureA.body.userData
        val obj2 = contact.fixtureB.body.userData

        if (obj1 is GameObject && obj2 is GameObject)
            beginContact(obj1 as GameObject, obj2 as GameObject)
    }

    final override fun preSolve(contact: Contact?, oldManifold: Manifold?) {
        val obj1 = contact!!.fixtureA.body.userData
        val obj2 = contact.fixtureB.body.userData

        if (obj1 is GameObject && obj2 is GameObject)
            preSolve(obj1 as GameObject, obj2 as GameObject)
    }

    final override fun postSolve(contact: Contact?, impulse: ContactImpulse?) {
        val obj1 = contact!!.fixtureA.body.userData
        val obj2 = contact.fixtureB.body.userData

        if (obj1 is GameObject && obj2 is GameObject)
            postSolve(obj1 as GameObject, obj2 as GameObject, impulse!!)
    }
}