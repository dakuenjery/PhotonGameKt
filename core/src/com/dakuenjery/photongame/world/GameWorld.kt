package com.dakuenjery.photongame.world

import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Disposable
import com.dakuenjery.photongame.objects.*
import com.dakuenjery.photongame.utils.Event
import org.greenrobot.eventbus.EventBus

/**
 * Created by dakue on 17/03/16.
 */

class GameWorld(worldController: Controller? = null) : Disposable {

    interface Controller {
        fun onWorldInited(world: GameWorld) {}
        fun onAdded(gm: GameObject?) {}
        fun onRemoved(gm: GameObject?) {}
    }

    private val WORLD_TIME_STEP: Float = 1/60f
    private var deltaAccumulator: Float = 0f

    val box2dWorld = World(Vector2.Zero, true)

    val components = Array<Component>()

    var timeScale: Float = 1f

//    val ray = Ray(box2dWorld)
//    val photon = GameObject.Photon(box2dWorld)

    private val contactLsnr = object : ContactListener() {
        override fun postSolve(obj1: GameObject, obj2: GameObject, impulse: ContactImpulse) {
            var photon: GameObject? = null
            var gm: GameObject? = null

            if (obj1 is GameObject.Photon) {
                photon = obj1

                if (obj2 is GameObject.Box)
                    gm = obj2
            } else if (obj2 is GameObject.Photon) {
                photon = obj2

                if (obj1 is GameObject.Box)
                    gm = obj1
            }

            if (photon != null)
                EventBus.getDefault().post(Event.PhotonTouched(photon.position, impulse))

            // Remove game object in next step
            if (gm != null) {
                removeComponent(gm)
                worldController?.onRemoved(gm)

                EventBus.getDefault().post(Event.DestroyBox(gm.position, impulse))
            }
        }
    }

    private var worldController: Controller? = null

    init {
        if (worldController != null)
            setCreator(worldController)

        box2dWorld.setContactListener(contactLsnr)
    }

    fun setCreator(worldController: Controller) {
        if (this.worldController != null)
            throw ExceptionInInitializerError("Controller setted")

        this.worldController = worldController
        worldController.onWorldInited(this)
    }

    fun addComponent(component: Component) {
        synchronized(components) {
            components.add(component)
        }
    }

    fun removeComponent(component: Component) {
        synchronized(components) {
            components.removeValue(component, true)
        }
    }

    fun update(delta: Float) {
        val stepTime = delta * timeScale
        deltaAccumulator += stepTime

        while (deltaAccumulator >= WORLD_TIME_STEP) {
            box2dWorld.step(stepTime, 4, 4)
            deltaAccumulator -= WORLD_TIME_STEP
        }

        synchronized(components) {
            for (comp in components)
                comp.update()
        }
    }

    override fun dispose() {
        box2dWorld.dispose()
    }
}

