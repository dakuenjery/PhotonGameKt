package com.dakuenjery.photongame

import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.dakuenjery.photongame.screens.GameScreen
import com.dakuenjery.photongame.screens.MainScreen
import com.dakuenjery.photongame.utils.GameEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class PhotonGame : com.badlogic.gdx.Game() {
    override fun create() {
        Gdx.app.logLevel = Application.LOG_DEBUG
        EventBus.builder()
                .logNoSubscriberMessages(false)
                .sendNoSubscriberEvent(false)
                .installDefaultEventBus()

        try {
            EventBus.getDefault().register(this)

            setScreen(MainScreen())
//            setScreen(GameScreen())

        } catch (ex: Exception) {
            Gdx.app.log("Fatal", "PhotonGame fatal error", ex)
        }
    }

    @Subscribe
    fun onEvent(ev: GameEvent) {
        if (ev == GameEvent.StartGame)
            setScreen(GameScreen())
    }
}

object GameConsts {
    val CAM_Z = 20f

    val STANDART_BLOCK_SIZE = Vector2(1f, 1f)
    val SMALL_BLOCK_SIZE = Vector2(0.5f, 0.5f)
    val TINY_BLOCK_SIZE = Vector2(0.3f, 0.3f)

    val OBJECT_SPAWN_POS = Vector2(25f, 25f)

    val WORLD_AMBIENT_COLOR = Color(.05f, .05f, .05f, .05f)
//    val WORLD_AMBIENT_COLOR = Color(.5f, .5f, .5f, .5f)

    val RAY_AMBIENT_COLOR = Color(.1f, .1f, .1f, 1f)
    val PHOTON_LIGHT_COLOR = Color(1f, 1f, 1f, 1f)
    val FONT_COLOR = Color(.3f, .3f, 1f, 1f)
    val GL_CLEAR_COLOR = Color(.1f, .1f, .1f, .1f)

    val PHOTON_SPEED = 10f

    val DRAW_DEBUG = false
    val BOX_DRAW_DEBUG = false
}