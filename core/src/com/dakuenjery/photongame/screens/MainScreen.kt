package com.dakuenjery.photongame.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.PointLight
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.objects.BodyModelProvider
import com.dakuenjery.photongame.objects.GameObject
import com.dakuenjery.photongame.render.GameCamera
import com.dakuenjery.photongame.render.Paintable
import com.dakuenjery.photongame.render.Painter
import com.dakuenjery.photongame.render.components.WorldPainter
import com.dakuenjery.photongame.utils.*
import com.dakuenjery.photongame.world.GameWorld
import org.greenrobot.eventbus.EventBus

/**
 * Created by dakue on 30/08/16.
 */

class MainScreen : BaseScreen() {
    private val camera = GameCamera()

    private val world: GameWorld
    private val painter: Painter
    private val ui: UI
    private val worldCreator: WorldController
    private val clearColor = Color(0f, 0f, 0f, .3f)
    private val hsv = Vector3(0f, 1f, 1f)

    protected val tasker = Tasker()

    init {
        worldCreator = WorldController()
        world = GameWorld(worldCreator)
        ui = UI()
        painter = Painter(camera, clearColor)
                .add(ui)
                .add(LightsPainter())
                .add(MainScreenWorldPainter())

        ui.playBtn.addListener(object : ClickListener() {
            override fun touchUp(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int) {
                if (isOver(ui.playBtn, x, y))
                    EventBus.getDefault().post(GameEvent.StartGame)
            }
        })

        tasker.addTask {
            ColorHalper.fromHSV(clearColor, hsv)
            hsv.x += 0.01f

            return@addTask false
        }

        Gdx.input.inputProcessor = ui.stage
    }

    override fun render(delta: Float) {
        try {
            world.update(delta)
            painter.paint(delta)

            tasker.proceedIfNeed(5)

            if (GameConsts.DRAW_DEBUG)
                super.render(delta)
        } catch (ex: Exception) {
            Gdx.app.debug("ERROR", "Render error", ex)
        }
    }

    override fun resize(width: Int, height: Int) {
        ui.stage.viewport.update(width, height, true)
        super.resize(width, height)
    }

    override fun dispose() {
        world.dispose()
        ui.dispose()
    }

    private inner class WorldController : GameWorld.Controller {
        private val compArr = Array<GameObject.Body>()
        private val forces = floatArrayOf(.6f, .8f, 0.7f, 0.5f)

        override fun onWorldInited(world: GameWorld) {
            val center = Vector2(0f, -2f)

            for (i in 1..15) {
                createBody(center, world, i)
            }

            var i = 0
            tasker.addTask {
                compArr.forEach { it.setVelocityAround(center, forces[i++ % forces.size]) }
                return@addTask false
            }

            world.timeScale = 0.2f
        }

        private fun createBody(center: Vector2, world: GameWorld, i: Int) {
            val box = GameObject.Body(world.box2dWorld, object : BodyModelProvider.Sphere(size = Vector2(.5f, .5f)) {
                override fun createModel(color: Color): Model? = null
            })
            box.init(Random.vector(-6f, -1f, 1f, 6f, i % 4).add(center))
            world.addComponent(box)
            compArr.add(box)
        }
    }

    private inner class MainScreenWorldPainter()
        : WorldPainter(camera, world) {
        override fun setup() {
            modelBatch = ModelBatch()
            env = Environment()
            pointLight = PointLight()
            colorAttr = ColorAttribute(ColorAttribute.AmbientLight, Color(.5f, .5f, .5f, 1f))
            pointLight.set(.6f, .6f, .6f, 0f, 0f, 20f, 100f)

            env.set(colorAttr)
            env.add(pointLight)
        }
    }

    private inner class LightsPainter()
        : com.dakuenjery.photongame.render.components.LightsPainter(camera, world) {
        init {
            createLight(Color.WHITE, 2048, 80f, 0f, -2f)
        }
    }

    private class UI: Paintable {
        val stage = Stage(ExtendViewport(480f, 800f))
        private val tex = Texture(Gdx.files.internal("data/playBtn.png"))
        val playBtn: ImageButton

        init {
            val sprtDrwbl = SpriteDrawable(Sprite(tex))

            playBtn = ImageButton(sprtDrwbl)

            val table = Table()
            table.debug = GameConsts.DRAW_DEBUG
            table.setFillParent(true)
            stage.addActor(table)

            table.add(playBtn)
                    .padTop(Value.percentHeight(0.2f, table))
                    .maxWidth(Value.percentWidth(0.4f, table))
                    .maxHeight(Value.percentWidth(0.4f, table))

        }

        override fun paint(delta: Float) {
            stage.act(delta)
            stage.draw()
        }

        override fun dispose() {
            stage.dispose()
            tex.dispose()
        }
    }
}