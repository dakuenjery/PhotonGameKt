package com.dakuenjery.photongame.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.render.components.FontPainter
import java.util.*

/**
 * Created by dakue on 16/03/16.
 */

open abstract class BaseScreen : Screen, InputAdapter() {

    private lateinit var fontPainter: FontPainter
    private var message: String = ""

    init {
        Gdx.input.inputProcessor = this
    }

    override fun show() { }
    override fun pause() { }
    override fun resize(width: Int, height: Int) {
        fontPainter = FontPainter(
                position = Vector2(20f, Gdx.graphics.height.toFloat() - 20f),
                color = Color.CYAN,
                fontsize = .3f
        )
    }

    override fun hide() { }

    override fun render(delta: Float) {
        fontPainter.text = "fps: ${Gdx.graphics.framesPerSecond}\n$message"
        fontPainter.paint(delta)
    }

    override fun resume() { }
    override fun dispose() {
        fontPainter.dispose()
    }

    fun debugMessage(message: String) {
        this.message = message
    }
}