package com.dakuenjery.photongame.screens

import box2dLight.DirectionalLight
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.badlogic.gdx.utils.Pool
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.objects.BodyModelProvider
import com.dakuenjery.photongame.objects.GameObject
import com.dakuenjery.photongame.render.GameCamera
import com.dakuenjery.photongame.render.Painter
import com.dakuenjery.photongame.render.components.BackgroundPainter
import com.dakuenjery.photongame.render.components.LightsPainter
import com.dakuenjery.photongame.render.components.WorldPainter
import com.dakuenjery.photongame.screens.BaseScreen
import com.dakuenjery.photongame.utils.CoordHelper
import com.dakuenjery.photongame.utils.Random
import com.dakuenjery.photongame.world.GameWorld
import com.dakuenjery.photongame.world.Ray

/**
 * Created by dakue on 16/03/16.
 */


class GameScreen : BaseScreen() {
    private val camera = GameCamera()

    private val world: GameWorld
    private val painter: Painter
    private val worldCreator: WorldContoller

    init {
        worldCreator = WorldContoller()

        world = GameWorld(worldCreator)
        painter = Painter(camera)
            .add(BackgroundPainter(camera, worldCreator.photon))
            .add(LightsPainter())
            .add(WorldPainter(camera, world, worldCreator.photon))

        for (i in 0..20)
            worldCreator.addBox(Random.vector(-.3f, .6f))

    }

    override fun render(delta: Float) {
        try {
            world.update(delta)
            painter.paint(delta)

            if (GameConsts.DRAW_DEBUG)
                super.render(delta)
        } catch (ex: Exception) {
            Gdx.app.debug("ERROR", "Render error", ex)
        }
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
//        val pos = CoordHelper.screenToRelative(camera, screenX.toFloat(), screenY.toFloat())
//        world.addBox(pos)

        val pos = CoordHelper.screenToRelative(camera, screenX.toFloat(), screenY.toFloat())
//        world.ray.addPoint(pos)
//        world.ray.init()

        worldCreator.ray.addPoint(pos)

        return super.touchDown(screenX, screenY, pointer, button)
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        val pos = CoordHelper.screenToRelative(camera, screenX.toFloat(), screenY.toFloat())
//        world.ray.addPoint(pos)
        worldCreator.ray.addPoint(pos)

        return super.touchDragged(screenX, screenY, pointer)
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        val pos = CoordHelper.screenToRelative(camera, screenX.toFloat(), screenY.toFloat())
//        worldCreator.addBox(pos)

        return super.touchUp(screenX, screenY, pointer, button)
    }

    override fun dispose() {
        world.dispose()
        painter.dispose()
    }

    private class WorldContoller : GameWorld.Controller {

        private lateinit var world: GameWorld

        private val gameObjectPool = object : Pool<GameObject>() {
            override fun newObject() = GameObject.Box(world.box2dWorld)
        }

        lateinit var photon: GameObject.Photon
        lateinit var ray: Ray

        override fun onWorldInited(world: GameWorld) {
            this.world = world

            createBorders().forEach {
                world.addComponent(it)
            }

            photon = GameObject.Photon(world.box2dWorld)
            ray = Ray(world.box2dWorld)

            world.addComponent(photon)
            world.addComponent(ray)

            photon.init(0f, -8f, 0f)
        }

        fun addBox(position: Vector2) {
            val pos = CoordHelper.relativeToWorld(position)
            val comp = gameObjectPool.obtain()
            comp.init(pos, angle = 0f)
            world.addComponent(comp)
        }

        override fun onRemoved(gm: GameObject?) {
            gameObjectPool.free(gm)
        }

        private fun createBorders(): List<GameObject> {
            val left = GameObject.Border(world.box2dWorld)
            left.init(-6.5f, 0f, 90f * MathUtils.degRad)

            val right = GameObject.Border(world.box2dWorld)
            right.init(6.5f, 0f, 90f * MathUtils.degRad)

            val up = GameObject.Border(world.box2dWorld)
            up.init(0f, 10f, 0f)

            val down = GameObject.Border(world.box2dWorld)
            down.init(0f, -10f, 0f)

            return listOf(left, right, up, down)
        }
    }

    private inner class LightsPainter() :
            com.dakuenjery.photongame.render.components.LightsPainter(camera, world)
    {
        init {
            createLight(Color.WHITE, 128, 20f, 0f, 0f, worldCreator.photon)

//            val dirLight = DirectionalLight(rayHandler, 64, Color(.9f, .9f, .9f, 1f), 0f)
//            lightArr.add(dirLight)

//            createLight(Color.WHITE, 32, 2f, 0f, -2f, screen.worldCreator.photon)
        }
    }

}
