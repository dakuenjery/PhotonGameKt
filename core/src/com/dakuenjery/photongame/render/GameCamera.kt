package com.dakuenjery.photongame.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.dakuenjery.photongame.GameConsts

/**
 * Created by dakue on 30/08/16.
 */

class GameCamera : PerspectiveCamera() {
    init {
        this.fieldOfView = 67f
        this.viewportWidth = Gdx.graphics.width.toFloat()
        this.viewportHeight = Gdx.graphics.height.toFloat()
        this.position.set(0f, 0f, GameConsts.CAM_Z)
        this.lookAt(0f, 0f, 0f)
        this.near = 0.1f
        this.far = 100f
    }
}