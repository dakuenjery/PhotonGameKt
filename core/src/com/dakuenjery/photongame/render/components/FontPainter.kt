package com.dakuenjery.photongame.render.components

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.render.Paintable

/**
 * Created by dakue on 30/08/16.
 */

class FontPainter(
        position: Vector2 = Vector2.Zero,
        text: String = "",
        color: Color = GameConsts.FONT_COLOR,
        fontsize: Float = 1f) : Paintable {
    private val batch = SpriteBatch()
    private val font = BitmapFont()

    var pos = Vector2(position)
        get() = field.cpy()
        set(value) { field.set(value) }

    var text = kotlin.text.String(text.toCharArray())
        get() = kotlin.text.String(field.toCharArray())
        set(value) { field = String(value.toCharArray()) }

    var color: Color
        get() = font.color
        set(value) { font.color = value }

    var fontsize: Float = fontsize
        get() = field
        set(value) {
            field = fontsize
            font.data.scale(field)
        }

    private val normalProjection = Matrix4()

    init {
        normalProjection.setToOrtho2D(0f, 0f,
                Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
        batch.projectionMatrix = normalProjection

        this.color = color
    }

    override fun paint(delta: Float) {
        batch.begin()
        font.draw(batch, text, pos.x, pos.y)
        batch.end()
    }

    override fun dispose() {
        batch.dispose()
        font.dispose()
    }
}