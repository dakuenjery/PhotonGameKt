package com.dakuenjery.photongame.render.components

import box2dLight.Light
import box2dLight.PointLight
import box2dLight.RayHandler
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Disposable
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.objects.GameObject
import com.dakuenjery.photongame.render.Paintable
import com.dakuenjery.photongame.world.GameWorld

/**
 * Created by dakue on 30/08/16.
 */

open class LightsPainter(val camera: Camera, world: GameWorld, attachTo: GameObject? = null) : Paintable {
    protected lateinit var rayHandler: RayHandler
    protected val lightArr = Array<Light>()

    init {
        RayHandler.setGammaCorrection(true)
        RayHandler.useDiffuseLight(true)

        rayHandler = createRayHanlder(world)
//        createLight(rayHandler)
    }

    open fun createRayHanlder(world: GameWorld) : RayHandler {
        val ray = RayHandler(world.box2dWorld)

        ray.setAmbientLight(GameConsts.RAY_AMBIENT_COLOR)
        ray.setBlur(true)
//        ray.setCulling(true)
        ray.setBlurNum(16)

        return ray
    }

    open fun createLight(color: Color = GameConsts.PHOTON_LIGHT_COLOR, rays: Int = 256,
                         distance: Float = 100f, x: Float = 0f, y: Float = 0f,
                         attachTo: GameObject? = null, ray: RayHandler = rayHandler): PointLight {
        val light = PointLight(ray, rays, color, distance, x, y)
        if (attachTo != null)
            light.attachToBody(attachTo.body)
        lightArr.add(light)
        return light
    }

    override fun paint(delta: Float) {
        rayHandler.setCombinedMatrix(camera.combined, 0f, 0f, camera.viewportWidth, camera.viewportHeight)
        rayHandler.updateAndRender()
    }

    override fun dispose() {
        rayHandler.dispose()
        lightArr.forEach { it.dispose() }
    }
}