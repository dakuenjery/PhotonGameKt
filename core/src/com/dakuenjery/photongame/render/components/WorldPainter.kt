package com.dakuenjery.photongame.render.components

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.PointLight
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.badlogic.gdx.utils.Array
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.objects.BodyModelProvider
import com.dakuenjery.photongame.objects.GameObject
import com.dakuenjery.photongame.render.Paintable
import com.dakuenjery.photongame.utils.Random
import com.dakuenjery.photongame.world.GameWorld
import java.util.*

/**
 * Created by dakue on 30/08/16.
 */

open class WorldPainter(val camera: Camera, val world: GameWorld, val attachBody: GameObject? = null) : Paintable {
    protected lateinit var modelBatch: ModelBatch
    protected lateinit var env: Environment
    protected lateinit var pointLight: PointLight
    protected lateinit var colorAttr: ColorAttribute

    private val box2dDebugRenderer = Box2DDebugRenderer(true, true, false, false, true, true)

    init {
        setup()
    }

    open fun setup() {
        modelBatch = ModelBatch()
        env = Environment()
        pointLight = PointLight()
        colorAttr = ColorAttribute(ColorAttribute.AmbientLight, GameConsts.WORLD_AMBIENT_COLOR)
        pointLight.set(.2f, .2f, .2f, 0f, 0f, 2f, 100f)

        env.set(colorAttr)
        env.add(pointLight)
    }

    override fun paint(delta: Float) {
        if (attachBody != null) {
            val lightPos = attachBody.position
            pointLight.setPosition(lightPos.x, lightPos.y, pointLight.position.z)
        }

        modelBatch.begin(camera)
        modelBatch.render(world.components, env)
        modelBatch.end()

        if (GameConsts.BOX_DRAW_DEBUG)
            box2dDebugRenderer.render(world.box2dWorld, camera.combined)

    }

    override fun dispose() {
        modelBatch.dispose()
    }
}