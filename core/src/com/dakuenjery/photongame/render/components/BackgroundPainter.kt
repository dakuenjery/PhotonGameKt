package com.dakuenjery.photongame.render.components

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.PointLight
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Array
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.objects.BodyModelProvider
import com.dakuenjery.photongame.objects.GameObject
import com.dakuenjery.photongame.render.Paintable
import com.dakuenjery.photongame.utils.Random

/**
 * Created by dakue on 19/09/16.
 */

open class BackgroundPainter(val camera: Camera, val attachBody: GameObject? = null) : Paintable {
    protected lateinit var modelBatch: ModelBatch
    protected lateinit var env: Environment
    protected lateinit var pointLight: PointLight
    protected val models = Array<ModelInstance>(100)
    protected lateinit var colorAttr: ColorAttribute

    init {
        modelBatch = ModelBatch()
        env = Environment()
        pointLight = PointLight()
        colorAttr = ColorAttribute(ColorAttribute.AmbientLight, Color(0f, .05f, .1f, .7f))
        pointLight.set(.2f, .2f, .2f, 0f, 0f, 2f, 100f)
        env.set(colorAttr)
        env.add(pointLight)
        setupBackground(BodyModelProvider.Box(size = Vector2(2f, 2f)))
    }


    open fun setupBackground(modelProvider: BodyModelProvider) {
        val model = modelProvider.createModel(Color.WHITE)

        // TO-DO: OPTIMIZE THIS!!!!
        for (i in -6..6) {
            for (j in -9..9) {
                val x = i * modelProvider.size.x
                val y = j * modelProvider.size.y
                val modelInstance = ModelInstance(model, x, y, -3f + Random.float(-.1f, .1f))
                models.add(modelInstance)
            }
        }
    }

    override fun paint(delta: Float) {
        if (attachBody != null) {
            val lightPos = attachBody.position
            pointLight.setPosition(lightPos.x, lightPos.y, pointLight.position.z)
        }

        modelBatch.begin(camera)
        modelBatch.render(models, env)
        modelBatch.end()
    }

    override fun dispose() {
        modelBatch.dispose()
    }
}