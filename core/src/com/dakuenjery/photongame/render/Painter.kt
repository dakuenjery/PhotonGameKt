package com.dakuenjery.photongame.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.Disposable
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.utils.Observer


/**
 * Created by dakue on 30/08/16.
 */

open class Painter(val camera: Camera, var clearColor: Color = GameConsts.GL_CLEAR_COLOR) : Observer<Paintable>(), Paintable {
    fun add(value: Paintable): Painter {
        addSubject(value)
        return this
    }

    override fun paint(delta: Float) {
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        camera.update()

        subjects.forEach {
            it.paint(delta)
        }
    }

    override fun dispose() = subjects.forEach {
        it.dispose()
    }
}

interface Paintable : Disposable {
    fun paint(delta: Float)
}