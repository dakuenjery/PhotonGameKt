package com.dakuenjery.photongame.objects

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.utils.Pool
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.utils.MathHelper
import com.dakuenjery.photongame.utils.Random

/**
 * Created by dakue on 29/05/16.
 */

sealed class GameObject : GravityComponent, Pool.Poolable
{
    constructor(box2dWorld: World, bodyModelProvider: BodyModelProvider = BodyModelProvider.BOX)
        : super(box2dWorld, bodyModelProvider)
    {
        reset()
    }

    inline fun init(position: Vector2, angle: Float = 0f) =
        init(position.x, position.y, angle)

    open fun init(x: Float, y: Float, angle: Float = 0f) {
        body.setTransform(x, y, angle)
        body.setLinearVelocity(0f, 0f)
        body.angularVelocity = 0f
        body.angularDamping = 0f
        body.linearDamping = 0f
        this.gravitateTo.set(x, y)

        body.isActive = true
    }

    // move over the visible scope and deactivate
    override fun reset() {
        body.isActive = false

//        body.setTransform(25f, 25f, 0f)
//        body.setLinearVelocity(0f, 0f)
//        body.angularVelocity = 0f
//        body.angularDamping = 0f
//        body.linearDamping = 0f
//        gravitateTo.set(0f, 0f)
    }



    class Border(box2dWorld: World) : GameObject(box2dWorld, BodyModelProvider.STATIC_LINE)

    class Box(box2dWorld: World) : GameObject(box2dWorld, BodyModelProvider.BOX)

    class Photon(box2dWorld: World) : GameObject(box2dWorld, BodyModelProvider.SPHERE)
    {
        override fun init(x: Float, y: Float, angle: Float) {
//            this.gravitateTo.set(Vector2.Zero)
            this.gravityScale = 0f
//            this.frictionScale = 0f

            super.init(x, y, angle)

            body.isBullet = true
            body.linearVelocity = Vector2(0f, 10f)
        }

        override fun update() {
//            val speed = MathHelper.distanse(Vector2.Zero, body.linearVelocity)
            val ox = MathHelper.cosAngleOx(Vector2.Zero, body.linearVelocity)
            val oy = MathHelper.cosAngleOy(Vector2.Zero, body.linearVelocity)

            body.setLinearVelocity(GameConsts.PHOTON_SPEED * oy, GameConsts.PHOTON_SPEED * ox)

            super.update()
        }
    }

    fun setVelocityAround(point: Vector2, scl: Float = 1f) {
        val vec2 = body.position.cpy().sub(point)
        vec2.set(-vec2.y, vec2.x).scl(scl)
        body.linearVelocity = vec2
    }

    class Body(box2dWorld: World, provider: BodyModelProvider) : GameObject(box2dWorld, provider) {}

    class DinamicBody(box2dWorld: World) : GameObject(box2dWorld, BodyModelProvider.Dynamic())
    {
        private val fixture = FixtureDef()
        private val polygonShape = PolygonShape()

        val fixtureCount: Int
            get() = body.fixtureList.size

        init {
            fixture.shape = polygonShape
            fixture.density = 1f
            fixture.restitution = 1f
        }

        fun clearFixtures() {
            while (body.fixtureList.count() > 0)
                body.destroyFixture(body.fixtureList[0])
        }

        fun removeFixture(id: Int = fixtureCount-1) {
            body.destroyFixture(body.fixtureList[id])
        }

        fun addFixture(shape: FloatArray, offset: Int = 0, len: Int = shape.size) {
            polygonShape.set(shape, offset, len)
            val fix = body.createFixture(fixture)

//            val strBuilder = StringBuilder()
//
//            for (i in offset..offset+len-1)
//                strBuilder.append("${shape[i]}, ")
//
//            Gdx.app.log("GameObject.addFixture", strBuilder.toString())
        }

        fun updateModel() {
            bodyModelProvider.mesh = bodyModelProvider.createMesh(body.fixtureList)
            modelInstance = ModelInstance(bodyModelProvider.createModel())
        }
    }
}


