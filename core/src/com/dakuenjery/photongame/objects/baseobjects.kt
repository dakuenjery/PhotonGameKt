package com.dakuenjery.photongame.objects

import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.Pool
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.utils.Random

/**
 * Created by dakue on 18/04/16.
 */

abstract class Component : RenderableProvider, Disposable {
    protected var modelInstance: ModelInstance? = null

    override fun getRenderables(renderables: Array<Renderable>?, pool: Pool<Renderable>?) {
        modelInstance?.getRenderables(renderables, pool)
    }

    open protected fun init() {}
    open fun update() {}

    override fun dispose() { modelInstance?.model?.dispose() }
}

open class PhisicsComponent(
        box2dWorld: World,
        val bodyModelProvider: BodyModelProvider
) : Component() {
    lateinit var body: Body

    val position: Vector2
        get() = body.position

    init {
        if (bodyModelProvider.bodyDef != null) {
            body = box2dWorld.createBody(bodyModelProvider.bodyDef)

            if (bodyModelProvider.fixture != null)
                body.createFixture(bodyModelProvider.fixture)

            body.userData = this
        }

        if (bodyModelProvider.model != null)
            modelInstance = ModelInstance(bodyModelProvider.model)
    }

    override fun update() {
        val position = body.position
        val degrees = Math.toDegrees(body.angle.toDouble()).toFloat()
        modelInstance?.transform?.setToRotation(0f, 0f, 1f, degrees)
        modelInstance?.transform?.setTranslation(position.x, position.y, 0f)
    }

    override fun dispose() {
        body.world.destroyBody(body)
        super.dispose()
    }
}

open class GravityComponent : PhisicsComponent
{
    private val gravityVec = Vector2()
    private val frictionVec = Vector2()
    protected var gravitateTo: Vector2 = Vector2()
    protected var gravityScale: Float = 0f
    protected var frictionScale: Float = 0f

    constructor(box2dWorld: World,
                bodyModelProvider: BodyModelProvider,
                gravitateTo: Vector2 = Vector2(bodyModelProvider.position),
                gravityScale: Float = 3f, frictionScale: Float = -.2f) :
        super(box2dWorld, bodyModelProvider)
    {
        this.gravitateTo = gravitateTo
        this.gravityScale = gravityScale
        this.frictionScale = frictionScale
    }

    override fun update() {
        if (gravityScale > 0)
            gravitateTo()
        super.update()
    }

    protected fun gravitateTo() {
        gravityVec.set(gravitateTo).sub(body.position)

        val distance = gravityVec.len2()

        frictionVec.set(body.linearVelocity).scl(frictionScale)
        gravityVec.add(frictionVec)

        if (distance > .1f) {
            gravityVec.scl(gravityScale/distance)
        }

        body.applyForceToCenter(gravityVec, true)
    }
}