package com.dakuenjery.photongame.objects

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Disposable
import com.dakuenjery.photongame.GameConsts
import com.dakuenjery.photongame.utils.MeshHelper
import com.dakuenjery.photongame.utils.Random

/**
 * Created by dakue on 26/08/16.
 */


sealed class BodyModelProvider(
        val position: Vector2,
        val size: Vector2,
        val bodyType: BodyDef.BodyType
) : Disposable
{
    companion object {
        val BOX = object : BodyModelProvider.Box() {}

        val SPHERE = object : BodyModelProvider.Sphere() {}

        val STATIC_LINE = object : BodyModelProvider.Box(
                size = Vector2(20f, 0.05f),
                bodyType = BodyDef.BodyType.StaticBody
        ) {}
    }

    open class Box(
            position: Vector2 = GameConsts.OBJECT_SPAWN_POS,
            size: Vector2 = GameConsts.STANDART_BLOCK_SIZE,
            bodyType: BodyDef.BodyType = BodyDef.BodyType.DynamicBody
    ) : BodyModelProvider(position, size, bodyType)
    {
        override fun createShape(): Shape {
            val shape = PolygonShape()
            shape.setAsBox(size.x/2, size.y/2)
            return shape
        }

        override fun createFixture(): FixtureDef {
            val fixtureDef = FixtureDef()
            fixtureDef.shape = shape
            fixtureDef.density = 1f
            fixtureDef.restitution = 1f
            return fixtureDef
        }

        override fun createModel(color: Color): Model? {
            val size = Vector3(size.x, size.y, 0.1f)
            val attrs = VertexAttributes.Usage.Position or VertexAttributes.Usage.Normal

            val mat = Material(ColorAttribute.createDiffuse(color))
            val builder = ModelBuilder()
            val mdl = builder.createBox(size.x, size.y, size.z, mat, attrs.toLong())

            return mdl
        }

        override fun dispose() {
            shape?.dispose()
        }
    }

    open class Sphere(
            position: Vector2 = GameConsts.OBJECT_SPAWN_POS,
            size: Vector2 = GameConsts.TINY_BLOCK_SIZE,
            bodyType: BodyDef.BodyType = BodyDef.BodyType.DynamicBody
    ) : BodyModelProvider(position, size, bodyType)
    {
        override fun createShape(): Shape {
            val shape = CircleShape()
            shape.radius = size.x / 2
            return shape
        }

        override fun createFixture(): FixtureDef {
            val fix = FixtureDef()
            fix.shape = shape
            fix.density = 1f
            fix.restitution = 1f

            return fix
        }

        override fun createModel(color: Color): Model? {
            val radius = size.x
            val attrs = VertexAttributes.Usage.Position or VertexAttributes.Usage.Normal

            val mat = Material(ColorAttribute.createDiffuse(Color.CYAN))
            val mdl = ModelBuilder().createSphere(radius, radius, radius, 24, 24, mat, attrs.toLong())
            return mdl
        }

        override fun dispose() {
            shape?.dispose()
        }
    }

    class Nullable(size: Vector2) : BodyModelProvider(Vector2.Zero, size, BodyDef.BodyType.StaticBody) {
        override fun createShape(): Shape {
            val shape = PolygonShape()
            shape.setAsBox(size.x/2, size.y/2)
            return shape
        }

        override fun createFixture(): FixtureDef {
            val fixtureDef = FixtureDef()
            fixtureDef.shape = shape
            return fixtureDef
        }

        override fun createModel(color: Color): Model? = null

        override fun dispose() {
            shape?.dispose()
        }
    }

    class Dynamic() : BodyModelProvider(Vector2.Zero, Vector2.Zero, BodyDef.BodyType.StaticBody) {
        private val tmpVec = Vector2()

        val mat = Material(ColorAttribute.createDiffuse(Color.RED))

        override fun createShape(): Shape? = null
        override fun createFixture(): FixtureDef? = null
        override fun createModel(color: Color): Model? {
            if (mesh == null)
                return null

            val modelBuilder = ModelBuilder()
            modelBuilder.begin()
            modelBuilder.part("", mesh, GL20.GL_TRIANGLES, mat)
            return modelBuilder.end()
        }

        override fun createMesh(fixtureList: Array<Fixture>): Mesh? {
            val meshHelper = MeshHelper(points = fixtureList.size * 4)

            for (fix in fixtureList) {
                val polyShape = fix.shape as PolygonShape

                for (i in 0..polyShape.vertexCount-1) {
                    polyShape.getVertex(i, tmpVec)
                    meshHelper.set(tmpVec)
                }

                meshHelper.index()
            }

            return meshHelper.create()
        }

        override fun dispose() {}
    }

    val bodyDef: BodyDef? = createBodyDef()
    val shape: Shape? = createShape()
    val fixture: FixtureDef? = createFixture()
    val model: Model? = createModel()
    var mesh: Mesh? = null

    open fun createBodyDef(): BodyDef? {
        val def = BodyDef()
        def.type = bodyType
        def.position.set(position)
        return def
    }

    abstract fun createShape(): Shape?
    abstract fun createFixture(): FixtureDef?
    abstract fun createModel(color: Color = Random.blueColor()): Model?

    open fun createMesh(fixtureList: com.badlogic.gdx.utils.Array<Fixture>): Mesh? = null
}
